# Mise en place d'une solution de supervision et de tests de charge pour Wordpress

Ce projet vise à déployer une solution de supervision et de tests de charge pour Wordpress en utilisant Azure CLI et Ansible, avec l'infrastructure hébergée sur GitLab.

## Configuration de l'infrastructure

- 2 VMs avec Wordpress (IaaS)
- 2 Bases de données MariaDB en redondance (SaaS)
- 1 Adresse IP publique
- 1 Load Balancer
- 1 Plugin Application Insight (Azure Monitor pour chaque WordPress)

## Instructions de déploiement

Pour mettre en place cette infrastructure, suivez les étapes ci-dessous :

1. Clonez le dépôt Git en utilisant la commande suivante :
git clone git@gitlab.com:alliance551908/haute-disponibilite-et-supervision.git

2. Accordez les droits d'exécution aux fichiers de déploiement avec la commande suivante :
chmod +x *

3. Exécutez la commande suivante pour lancer le déploiement :
`./brief.sh`

Pour plus de détails sur les scripts de déploiement, consultez les explications fournies dans les fichiers `x.sh.README.md` présents dans ce dépôt.
