#!/bin/bash

# Charger les variables depuis variable.sh
source vars.sh

# installation de Apache 2
echo "installation de Apache 2"     
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "sudo apt update; sudo apt -y install apache2 ; sudo systemctl restart apache2"}' \
    || handle_failure

# installation de PHP
echo "installation de PHP"    
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "sudo apt update; sudo apt -y install php ; sudo apt -y install php-mysql php-curl php-gd php-zip ;sudo systemctl restart apache2 "}' \
    || handle_failure

# installation des dépendances WordPress
echo "installation des dépendances WordPress"     
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "sudo apt update ; sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip ; sudo systemctl restart apache2"}' \
    || handle_failure

# Installation de WordPress 
echo "start installation de WP"
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "sudo apt update;  cd /var/www/html/ ; sudo wget https://wordpress.org/latest.tar.gz ;  sudo tar -xzvf latest.tar.gz  "}' \
    || handle_failure
echo "installation de WP faite"

# Renomme le répertoire en karim-site 
echo "start Renomme le repertoire"
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": " cd /var/www/html ; sudo  mv wordpress karim-site ;  cd karim-site ;  sudo cp wp-config-sample.php wp-config.php  "}' \
    || handle_failure
echo "Renommage fait"

# configuration de wp-config.php
echo "start configuration de wp"
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "sudo sed -i 's/database_name_here/wordpressdb/' /var/www/html/karim-site/wp-config.php; sudo sed -i 's/username_here/wpuser/' /var/www/html/karim-site/wp-config.php;  sudo sed -i 's/password_here/P@ssw0rd4567/' /var/www/html/karim-site/wp-config.php; sudo sed -i 's/localhost/10.0.15.52/' /var/www/html/karim-site/wp-config.php "}' \
    || handle_failure

# Configuration Apache 2
az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": " sudo chown -R www-data:www-data /var/www/html/karim-site ; sudo chmod -R 755 /var/www/html/karim-site ;   cd /etc/apache2/sites-available/ "}' \
    || handle_failure

az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": "  echo \"<VirtualHost *:80>\n  ServerName exemple1.lan\n  DocumentRoot /var/www/html/karim-site/\n</VirtualHost>\" >> /etc/apache2/sites-available/karim-site.conf"}' \
    || handle_failure

az vm extension set \
    --resource-group $resourceGroup \
    --vm-name $VmWeb1Name --name customScript \
    --publisher Microsoft.Azure.Extensions \
    --settings '{"commandToExecute": " cd  ;  sudo a2ensite karim-site.conf ;  sudo a2dissite 000-default.conf   ;  sudo systemctl restart apache2  "}' \
    || handle_failure

# Ignorer les avertissements HTTPS, y compris la validation du fingerprint
echo "Ignorer les avertissements HTTPS..."
export AZURE_CLI_IGNORE_HTTPS_WARNINGS=true

# Récupérer l'adresse IP publique associée à VmWeb1
PublicIp1=$(az network public-ip show --resource-group $resourceGroup --name $Vm1PublicIp --query ipAddress -o tsv)

# Copie du dossier insights vers la VM
echo "Copie du dossier insights..."
# Copier les fichiers sur la machine virtuelle dans un répertoire temporaire où l'utilisateur a les permissions
scp -r -o StrictHostKeyChecking=no -i /home/arnaud/.ssh/id_rsa $localPath $adminUsername@$PublicIp1:~/temp_destination || handle_failure
# Se connecter à la machine virtuelle et exécuter sudo mv ou sudo cp pour déplacer/copier les fichiers dans l'emplacement final
ssh -o StrictHostKeyChecking=no -i /home/arnaud/.ssh/id_rsa $adminUsername@$PublicIp1 "sudo mv ~/temp_destination $Destination" || handle_failure

# Récupérer l'ID de la carte réseau de la VM
nicId1=$(az vm show -g $resourceGroup -n $VmWeb1Name --query 'networkProfile.networkInterfaces[0].id' -o tsv)
nicName1=$(az network nic show --ids $nicId1 --query 'name' -o tsv)

# Détacher l'adresse IP publique de la carte réseau
echo "Dissociation de l'adresse IP publique de la VM web 1..."
az network nic ip-config update --nic-name $nicName1 --name ipconfiglb-VM1 -g $resourceGroup --remove publicIpAddress || handle_failure

# Supprimer l'adresse IP publique associée à VmWeb1
echo "Suppression de l'adresse IP publique de la VM web 1..."
az network public-ip delete -g $resourceGroup --name $Vm1PublicIp || handle_failure
