#!/bin/bash

# Charger les variables depuis variable.sh
source vars.sh

# Création de l'Ip Public Load-Balancer
echo "Création de l'Ip Public Load-Balancer..."
az network public-ip create \
--resource-group $resourceGroup \
--location $region \
--name $lbPublicIP \
--sku $lbPublicIPSKU \
--zone $lbPublicIPZone \
|| handle_failure

# Création du Load-Balancer
echo "Création du Load-Balancer..."
az network lb create \
--resource-group $resourceGroup \
--location $region \
--name $lbName \
--sku $lbSku \
--public-ip-address $lbPublicIP \
--frontend-ip-name $lbfrontendIp \
--public-ip-zone $frontendIpZone \
--backend-pool-name $backendPoolName \
|| handle_failure

az network lb probe create \
--resource-group $resourceGroup \
--lb-name $lbName \
--name $healthProbeName \
--protocol $protocolProbe \
--port $probePort \
|| handle_failure

az network lb rule create \
--resource-group $resourceGroup \
--lb-name $lbName \
--name $lbRuleName \
--protocol $lbRuleProtocol \
--frontend-port $frontendPort \
--backend-port $backendPort \
--frontend-ip-name $lbfrontendIp \
--backend-pool-name $backendPoolName \
--probe-name $healthProbeName \
--disable-outbound-snat $disableOutboundSnat \
--idle-timeout $idleTimeout \
--enable-tcp-reset $enableTcpReset \
|| handle_failure

# Récupérer l'adresse IP associée aux VMs
VmWeb1Ip=$(az network nic show --ids $(az vm show -g $resourceGroup -n lb-VM1 --query "networkProfile.networkInterfaces[0].id" -o tsv) --query ipConfigurations[].privateIPAddress -o tsv)
VmWeb2Ip=$(az network nic show --ids $(az vm show -g $resourceGroup -n lb-VM2 --query "networkProfile.networkInterfaces[0].id" -o tsv) --query ipConfigurations[].privateIPAddress -o tsv)

# Récupérer l'id des subnets
subnetLbWeb1Id=$(az network vnet subnet show --resource-group $resourceGroup --vnet-name $vnetLbName --name $subnetLbWeb1Name --query id -o tsv)
subnetLbWeb2Id=$(az network vnet subnet show --resource-group $resourceGroup --vnet-name $vnetLbName --name $subnetLbWeb2Name --query id -o tsv)

# Ajouter backend web 1 au pool d'adresses du load balancer
echo "Ajout de backend web 1 au pool d'adresses du load balancer..."
az network lb address-pool address add \
--resource-group $resourceGroup \
--lb-name $lbName \
--pool-name $backendPoolName \
--name $VmWeb1Name \
--subnet $subnetLbWeb1Id \
--ip-address $VmWeb1Ip \
|| handle_failure

# Ajouter backend web 2 au pool d'adresses du load balancer
echo "Ajout de backend web 2 au pool d'adresses du load balancer..."
az network lb address-pool address add \
--resource-group $resourceGroup \
--lb-name $lbName \
--pool-name $backendPoolName \
--name $VmWeb2Name \
--subnet $subnetLbWeb2Id \
--ip-address $VmWeb2Ip \
|| handle_failure