#!/bin/bash

# Charger les variables depuis variable.sh
source vars.sh

# Récupérer l'id des subnets
subnetLbDb1Id=$(az network vnet subnet show --resource-group $resourceGroup --vnet-name $vnetLbName --name $subnetLbDb1Name --query id -o tsv)

# Création de la base de données MySQL (Azure Database for MySQL)
echo "Création de la base de données MySQL (Azure Database for MySQL)"
az mysql flexible-server create \
--resource-group $resourceGroup \
--subnet $subnetLbDb1Id \
--name $FlexiServerName \
--yes \
--location $region \
--database-name $dbName \
--admin-user $wp_db_user \
--admin-password $wp_db_password \
--sku-name $dbSku \
--tier $dbtier \
--high-availability $HighAvailability \
|| handle_failure



#Une fois déployé, je désactive require_secure_transport dans les paramètres du serveur flexible Azure. Pour ce faire, je me rends sur l'interface graphique d'Azure, j'accède au serveur flexible, je clique sur "Settings Server" dans le menu de gauche, puis dans la barre de recherche je tape "secur" et je clique sur "off" pour désactiver "require_secure_transport".