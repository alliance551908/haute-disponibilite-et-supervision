#!/bin/bash

# Charger les variables depuis variable.sh
source vars.sh

# Création du test de charge Azure
echo "Création du test de charge Azure"
az load create \
--name $loadTestResource \
--resource-group $resourceGroup \
--location $region \
|| handle_failure

# Création de Insights
echo "Création de Insights"
az monitor app-insights api-key create \
--api-key $InsightsKeykarim \
--app $InsightsApp \
--resource-group $resourceGroup \
|| handle_failure

#Pour finaliser l'installation d'Insights, je vais dans l'interface graphique de WordPress de chaque VM. Dans le menu de gauche, je clique sur "Plugins", puis je coche la case pour activer "Application Insights". Ensuite, je récupère la clé d'instrumentation à partir de l'interface graphique d'Azure. Pour cela, je me rends dans la ressource Application Insights, puis dans la vue d'ensemble en haut à droite.

##Pour lancer un test de charge, je vais dans l'interface graphique d'Azure, j'accède à l'outil de test de charge. Ensuite, dans le menu de gauche, je clique sur "Test", puis dans le champ de recherche, je saisis une URL en tapant "http://{ippubliqueloadbalancer}".