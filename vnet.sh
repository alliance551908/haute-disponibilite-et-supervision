#!/bin/bash

# Charger les variables depuis variable.sh
source vars.sh

# Création du groupe de ressource
echo "Création du groupe de ressource..."
az group create \
    --location $region \
    --name $resourceGroup || handle_failure

# Création du NSG
echo "Création du NSG..."
az network nsg create \
--resource-group $resourceGroup \
--name $nsgName \
|| handle_failure

az network nsg rule create \
--resource-group $resourceGroup \
--nsg-name $nsgName \
--name $nsgRuleName \
--protocol "$nsgProtocole" \
--direction $ruleHTTPDirection \
--source-address-prefix "$ruleHTTPsourceAddressPrefix" \
--source-port-range "$ruleHTTPsourcePortRange" \
--destination-address-prefix "$ruleHTTPdestinationAdressPrefix" \
--destination-port-range $ruleHTTPdestinationPortRange \
--access $ruleHTTPaccess \
--priority $ruleHTTPpriority \
|| handle_failure

az network nsg rule create \
--resource-group $resourceGroup \
--nsg-name $nsgName \
--name $nsgSSHRuleName \
--protocol "$nsgSSHProtocole" \
--direction $ruleSSHDirection \
--source-address-prefix "$ruleSSHsourceAddressPrefix" \
--source-port-range "$ruleSSHsourcePortRange" \
--destination-address-prefix "$ruleSSHdestinationAdressPrefix" \
--destination-port-range $ruleSSHdestinationPortRange \
--access $ruleSSHaccess \
--priority $ruleSSHpriority \
|| handle_failure

# Création du réseau virtuel
echo "Création du VNet..."
az network vnet create \
    --resource-group $resourceGroup \
    --name $vnetLbName \
    --address-prefixes $vnetAddressPrefix \
    --location $region \
    || handle_failure

# Création du sous-réseau backend web 1
echo "Création du subnet web 1..."
az network vnet subnet create \
    --resource-group $resourceGroup \
    --vnet-name $vnetLbName \
    --name $subnetLbWeb1Name \
    --nsg $nsgName \
    --address-prefix $subnetLbWeb1AddressPrefix || handle_failure

# Création du sous-réseau backend web 2
echo "Création du subnet web 2..."
az network vnet subnet create \
    --resource-group $resourceGroup \
    --vnet-name $vnetLbName \
    --name $subnetLbWeb2Name \
    --nsg $nsgName \
    --address-prefix $subnetLbWeb2AddressPrefix || handle_failure

# Création du sous-réseau backend db 1
echo "Création du subnet db 1..."
az network vnet subnet create \
    --resource-group $resourceGroup \
    --vnet-name $vnetLbName \
    --name $subnetLbDb1Name \
    --nsg $nsgName \
    --address-prefix $subnetLbDb1AddressPrefix || handle_failure
