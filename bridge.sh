#!/bin/bash

# Charger les variables depuis variable.sh
source vars.sh

# Création de l'adresse IP publique
az network public-ip create --resource-group $resourceGroup --name $publicIpAddressName --sku Standard --location $region

# Déploiement de la passerelle NAT
az network nat gateway create \
    --name $natGatewayName \
    --resource-group $resourceGroup \
    --public-ip-address $publicIpAddressName \
    --location $region \
    --idle-timeout $idleTimeoutInMinutes \
    || handle_failure