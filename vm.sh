#!/bin/bash

# Charger les variables depuis variable.sh
source vars.sh

# Création de la VM web 1
echo "Création de la machine virtuelle web 1..."
az vm create \
--resource-group $resourceGroup \
--name $VmWeb1Name \
--image $imageID \
--size $sizeVM \
--storage-sku $storageSKU \
--nsg "" \
--public-ip-address $Vm1PublicIp \
--vnet-name $vnetLbName \
--subnet $subnetLbWeb1Name \
--admin-username $adminUsername \
--generate-ssh-keys \
--verbose || handle_failure

# Création de la VM web 2
echo "Création de la machine virtuelle web 2..."
az vm create \
--resource-group $resourceGroup \
--name $VmWeb2Name \
--image $imageID \
--size $sizeVM \
--storage-sku $storageSKU \
--nsg "" \
--public-ip-address $Vm2PublicIp \
--vnet-name $vnetLbName \
--subnet $subnetLbWeb2Name \
--admin-username $adminUsername \
--generate-ssh-keys \
--verbose || handle_failure