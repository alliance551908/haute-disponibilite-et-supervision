#!/bin/bash

# Exécution du script vnet.sh
echo "Exécution du script vnet.sh..."
bash vnet.sh

# Attente de quelques secondes pour s'assurer que les ressources réseau sont prêtes avant de créer la VM
sleep 30

# Exécution du script bridge.sh
echo "Exécution du script bridge.sh..."
bash bridge.sh

sleep 30

# Exécution du script database.sh
echo "Exécution du script database.sh..."
bash database.sh

sleep 30

# Exécution du script vm.sh
echo "Exécution du script vm.sh..."
bash vm.sh

sleep 30

# Exécution du script provid1.sh
echo "Exécution du script provid1.sh..."
bash provid1.sh

sleep 30

# Exécution du script provid2.sh
echo "Exécution du script provid2.sh..."
bash provid2.sh

# Exécution du script lb.sh
echo "Exécution du script lb.sh..."
bash lb.sh

# Exécution du script monitoring.sh
echo "Exécution du script monitoring.sh..."
bash monitoring.sh